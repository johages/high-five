# Welcome to Remix!

- [Remix Docs](https://remix.run/docs)

## Development

From your terminal:

```sh
pnpm run dev
```

This starts your app in development mode, rebuilding assets on file changes.

## Deployment

First, build your app for production:

```sh
pnpm run build
```

Then run the app in production mode:

```sh
pnpm start
```

Now you'll need to pick a host to deploy it to.

## Docker

Run MONGODB first:

```
docker run -d --name high-five-db \
> -e MONGO_INITDB_ROOT_USERNAME=**** \
> -e MONGO_INITDB_ROOT_PASSWORD=**** \
> -p 27017:27017 -v highfivedb:/data/db \
> mongo:7.0
```

First build the docker image:

```sh
docker build -t high-five . --build-arg CONN="mongodb://****:****@localhost:27017/"
```

Then run the docker image:

```sh
docker run -dp 127.0.0.1:3000:3000 high-five
```


### DIY

If you're familiar with deploying node applications, the built-in Remix app server is production-ready.

Make sure to deploy the output of `remix build`

- `build/`
- `public/build/`
