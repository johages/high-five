import { createCookie } from "@remix-run/node"; // or cloudflare/deno

export const userCookie = createCookie("user", {
  maxAge: 604_800, // one week
});
