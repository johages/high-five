import { json, redirect } from "@remix-run/node";
import type { MetaFunction, LoaderFunctionArgs } from "@remix-run/node";
import { useLoaderData, Form } from "@remix-run/react";
import { mongodb, ObjectId } from "~/utils/db.server";
import type { UserDocument, UserType } from "~/utils/types.server"
import User from "~/components/user";
import { userCookie } from "~/cookies.server";

export async function loader({ request }: LoaderFunctionArgs) {
  const cookieHeader = request.headers.get("Cookie");
  const cookie =
    (await userCookie.parse(cookieHeader)) || {};
  const currentUserId = new ObjectId(cookie.id);

  const db = await mongodb.db("highfive");
  const collection = await db.collection<UserDocument>("fives");
  const users = await collection.find({ _id: { $ne: currentUserId }}).toArray();
  const currentUser = await collection.findOne({ _id: currentUserId });
  return json({users, currentUser});
}

export async function action() {
  const user: UserType = {
    status: "online",
    received: []
  }
  const db = await mongodb.db("highfive");
  const collection = await db.collection("fives");
  const result = await collection.insertOne(user);
  const userId = result.insertedId;
  return redirect("/", {
    headers: {
      "Set-Cookie": await userCookie.serialize({ id: userId }),
    },
  })
}

export const meta: MetaFunction = () => {
  return [
    { title: "New Remix App" },
    { name: "description", content: "Welcome to Remix!" },
  ];
};

export default function Index() {
  const {users, currentUser } = useLoaderData<typeof loader>();
  return (
    <div style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.8" }}>
      <h1>Welcome to Remix</h1>
      {users.map((user) => {
        return (
          <User key={user._id} status={user.status} fives={user.received} />
        )
      })}
      {
        currentUser ? 
          <div>Current user: {JSON.stringify(currentUser)}</div>
           :
          <div>
            <h2>Join</h2>
            <Form method="POST">
              <button type="submit">
                Join
              </button>
            </Form>
          </div>
      }
    </div>
  );
}
