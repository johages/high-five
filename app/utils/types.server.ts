import type { WithId, Document } from "mongodb";

export interface Fives {
    date: string
}

export type Status =  "online" | "offline";

export interface UserType {
  status: Status,
  received: Fives[]
}

export interface UserDocument extends WithId<Document>, UserType {}
