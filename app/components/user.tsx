import type { Status, Fives } from "~/utils/types.server"

export default function User ({ status }: { status: Status, fives: Fives[] }) {
    return (
        <div>
            <strong>Status:</strong> {status}
        </div>
    );
}